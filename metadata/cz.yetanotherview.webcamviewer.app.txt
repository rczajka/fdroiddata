Categories:Internet,Multimedia
License:Apache2
Web Site:https://github.com/TomasValenta/WebCamViewer/blob/HEAD/README.md
Source Code:https://github.com/TomasValenta/WebCamViewer
Issue Tracker:https://github.com/TomasValenta/WebCamViewer/issues

Auto Name:WebCam Viewer
Summary:View webcam streams
Description:
Add and refresh webcams accessible from an URL.
.

Repo Type:git
Repo:https://github.com/TomasValenta/WebCamViewer

Build:2.0,25
    commit=fac88fdc9619330497f8554a548b22c63ad707a4
    subdir=app
    gradle=yes

Build:2.0 beta2,26
    commit=6e17acb5a72ea3ac8fb533896a902ebfcc9938ed
    subdir=app
    gradle=yes

Build:2.0 beta2,28
    commit=441842c48d3631393cdf3b96fdb31337dda129a9
    subdir=app
    gradle=yes

Build:2.0 beta3,29
    commit=d463f62340d8085b967c671960d50a0067284eda
    subdir=app
    gradle=yes

Build:2.0 beta3,30
    commit=0c0431d81dfcf53daaff488bb6b865449721eb3e
    subdir=app
    gradle=yes

Build:2.0 beta4,31
    commit=v2.0-beta.4
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0 beta5
Current Version Code:32

